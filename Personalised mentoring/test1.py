import gradio as gr
import openai
import json

# Load the JSON file
json_file_path = 'E:/beyond exam/be-projects/Personalised mentoring/p1.json'

with open(json_file_path, 'r') as file:
    json_data = json.load(file)

# Extract data from JSON
topic = json_data['topic']
description = json_data['description']
key_skills = json_data['key_skills']
Internet = json_data['Internet']
HTML = json_data['HTML']
CSS = json_data['CSS']
Javascript = json_data['Javascript']

# Initialize OpenAI ChatGPT
openai.api_key = open("E:/beyond exam/be-projects/Personalised mentoring/key.txt", "r").read().strip('\n')
chatgpt_model = 'gpt-3.5-turbo'

# Define a function to interact with ChatGPT
def chat_with_gpt(message):
    if "frontend" in message.lower():
        return description
    elif "skill" in message.lower():
        return " ".join(key_skills)
    elif "internet" in message.lower():
        return " ".join(Internet)
    elif "html" in message.lower():
        return " ".join(HTML)
    elif "css" in message.lower():
        return " ".join(CSS)
    elif "javascript" in message.lower():
        return " ".join(Javascript)
    else:
        response = openai.ChatCompletion.create(
        model=chatgpt_model,
        messages=[
            {"role": "system", "content": f"I have some data to extract:\nTopic: {topic}\nDescription: {description}"},
            {"role": "user", "content": message}
        ]
    )
    return response.choices[0].message.content.strip()

# Define the chat interface
def chat_interface(message):
    # Send user's message to ChatGPT and handle data-specific responses
    response = chat_with_gpt(message)

    return response

# Create the Gradio interface
iface = gr.Interface(
    fn=chat_interface,
    inputs="text",
    outputs="text",
    title="ChatGPT with Data Extraction",
    description="Enter your message to chat with ChatGPT start with learn then type topic."
)

if __name__ == '__main__':
    iface.launch()
